define(['custom'], function(custom) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $firebase, psResponsive) {	   
				    
					// INIT
					$scope.init = function(){
						 cfpLoadingBar.start();
					};	
					
					$timeout(function(){
						 cfpLoadingBar.complete();
					}, 1000);
				
					$scope.lorem = "Lorem ipsum dolor sit amet, an epicuri mediocrem vituperata eos. Illud volumus periculis per no, cu torquatos definitionem eum. Est cu hendrerit vituperatoribus. In officiis nominati eum, aperiri dolorem usu ut. In meliore denique suavitate vim.";
					
				


				    // ROW SLIDER
				    $scope.rowSlider = [
				    	{
					    	title: "Product 1 | Product 2 | Product 3",					    	
					    	image: 'media/tools/row1.png'
				    	},
				    	{
					    	title: "Product 4 | Product 5 | Product 6",					    	
					    	image: 'media/tools/row2.png'
				    	},
				    	{
					    	title: "Product 7 | Product 8 | Product 9",					    	
					    	image: 'media/tools/row3.png'
				    	}				    		
				    	
				    ];
				    $scope.slideIndex = 0; 
		            // demo with controls
		            $scope.prev = function() {
		                $scope.slideIndex--;
		                
		                if ( $scope.slideIndex < 0){
		                	$scope.slideIndex = $scope.rowSlider.length - 1;
		                }
		            };
		            $scope.next = function() {
		                $scope.slideIndex++;
		                if ($scope.slideIndex > $scope.rowSlider.length - 1){
		                	$scope.slideIndex = 0;
		                }
		            };					    
				    	
				    
				    // ROW SLIDER
				    $scope.peopleSlider = [
				    	{
					    	title: "M. Bison | President",					    	
					    	image: 'http://lorempixel.com/300/300/people/1'
				    	},
				    	{
					    	title: "Vega | Vice President",			    	
					    	image: 'http://lorempixel.com/300/300/people/8'
				    	},
				    	{
					    	title: "Balrog | Grunt",			    	
					    	image: 'http://lorempixel.com/300/300/people/5'
				    	}				    		
				    	
				    ];	
				    $scope.peopleIndex = 0; 
		            // demo with controls
		            $scope.prev_p = function() {
		                $scope.peopleIndex--;
		                if ( $scope.peopleIndex < 0){
		                	$scope.peopleIndex = $scope.peopleSlider.length - 1;
		                }
		            };
		            $scope.next_p = function() {
		                $scope.peopleIndex++;
		                if ($scope.peopleIndex > $scope.peopleSlider.length - 1){
		                	$scope.peopleIndex = 0;
		                }
		            };	
				    


				    
				    
				    
				    
          
            
	    
				});				
	    },
	    ///////////////////////////////////////
  };
});
